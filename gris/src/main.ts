import './app.css'
import 'ol/ol.css';
import App from './App.svelte'

const app = new App({
  target: document.getElementById('app')
  
})

export default app
