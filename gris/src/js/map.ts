import { writable } from 'svelte/store';
import { Map, View } from "ol";
import { OSM, Vector as VectorSource } from "ol/source";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { Circle as CircleStyle, Fill, Style } from "ol/style";
import { transform } from "ol/proj";
import GeoJSON from "ol/format/GeoJSON";

function mapInit() {
    return new Map({
        target: "map",
        layers: [
            new TileLayer({
                source: new OSM(),
            }),
        ],
        view: new View({
            center: transform([30.52, 50.45], 'EPSG:4326', 'EPSG:3857'),
            zoom: 5,
        }),
    });
}

export const createVectorLayerGeoJson = (s) => {
    return new VectorLayer({
        source: new VectorSource({
            features: (new GeoJSON({
                featureProjection: 'EPSG:3857'
            })).readFeatures(s)
        }),
        style: new Style({
            image: new CircleStyle({
                radius: 3,
                fill: new Fill({
                    color: "rgba(255,0,0,1)",
                }),
            }),
        }),
    });
}


function createMap() {
    const { subscribe, set, update } = writable(mapInit);

    return {
        subscribe,
        init: async () => {
            return new Map({
                target: "map",
                layers: [
                    new TileLayer({
                        source: new OSM(),
                    }),
                ],
                view: new View({
                    center: transform([48.23059554404654, 21.17722359958838], 'EPSG:4326', 'EPSG:3857'),
                    zoom: 5,
                }),
            });
        },
        update
    };
}

export const map = createMap();