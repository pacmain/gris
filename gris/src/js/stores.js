import { writable } from 'svelte/store';

function createTimeFrame() {
    const { subscribe, set, update } = writable({
        current: 0,
        window: 0,
        min: 0,
        max: 0,
        play: false,
    });

    return {
        subscribe,
        increment: (t) => update(n => {
            n.current += t
            return n
        }),
        decrement: (t) => update(n => {
            n.current -= t
            return n
        }),
        jump: (t) => update(n => {
            n.current = t
            return n
        }),
        togglePlay: () => update(n => {
            n.play = !n.play
            return n
        }),
        reset: (c, w, mi, ma) => set({
            current: c,
            window: w,
            min: mi,
            max: ma,
            play: false,
        })
    };
}

export const timeline = createTimeFrame();

function createPopup() {
    const { subscribe, set, update } = writable({

    })

    return {
        subscribe,
        set,
    }
}

export const popup = createPopup();