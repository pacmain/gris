import datetime
import time
from geojson import Feature, Point, FeatureCollection
import random
import json

coords = [(round(random.uniform(5.00000, 50.00000), 5), round(random.uniform(40.00000, 70.00000), 5)) for _ in range(10000)]

result = []

now = datetime.datetime.now()



for idx, coord in enumerate(coords):
    f = Feature(geometry=Point(coord))
    f['properties'] = {
        'id': idx,
        'timestamp': time.mktime((now - datetime.timedelta(minutes=idx) + datetime.timedelta(minutes=random.randint(1, 3))).timetuple()),
        'meta_1': "kfölsdkflsdkfpowek",
        'meta_2': "kfölsdkflsdkfpowek",
        'meta_3': {'test': 1},
        'meta_5': "true",
        'meta_6': 67.6,
        'meta_7': "dfsdfsk",
        'meta_8': "kfölsdkflsdkfpowek",
    }
    result.append(f)

collection = FeatureCollection(result)

with open('geojson.json', 'w') as file:
    json.dump(collection, file)